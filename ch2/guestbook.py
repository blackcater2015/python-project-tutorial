# coding: utf-8
import shelve
from datetime import datetime
from flask import \
    Flask,\
    request, \
    render_template, \
    redirect, \
    escape, \
    Markup

application = Flask(__name__)

DATA_FILE = 'guestbook.dat'


def save_data(name, comment, create_at):
    """
    保存数据
    :param name: 用户名称
    :param comment: 评论内容
    :param create_at: 评论时间
    :return:
    """
    database = shelve.open(DATA_FILE)
    if 'greeting_list' not in database:
        greeting_list = []
    else:
        greeting_list = database['greeting_list']

    greeting_list.insert(0, {
        'name': name,
        'comment': comment,
        'create_at': create_at
    })

    database['greeting_list'] = greeting_list
    database.close()


def load_data():
    """
    获取评论数据
    :return: 评论数据
    """
    database = shelve.open(DATA_FILE)
    greeting_list = database.get('greeting_list', [])
    database.close()
    return greeting_list


@application.template_filter('nl2br')
def nl2br_filter(s):
    return escape(s).replace('\n', Markup('<br/>'))


@application.template_filter('datetime_fmt')
def datetime_fmt_filter(dt):
    return dt.strftime('%Y-%m-%d %H:%M:%S')


@application.route('/post', methods=['post'])
def post():
    name = request.form.get('name')
    comment = request.form.get('comment')
    create_at = datetime.now()
    save_data(name, comment, create_at)

    return redirect('/')


@application.route('/')
def index():
    """
    首页
    :return: 使用模板显示页面
    """
    greeting_list = load_data()

    return render_template('index.html', greeting_list=greeting_list)


if __name__ == '__main__':
    application.run('127.0.0.1', 8080, debug=True)
